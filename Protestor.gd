extends PathFollow2D

export var parent = ""

func _ready():
	parent = get_node("../../" + parent)
	call_deferred("set_parent", parent)
	

func _process(delta):
	if (unit_offset < 1):
		set_unit_offset(unit_offset + .1)
	

func set_parent(newparent):
	get_parent().remove_child(self)
	newparent.add_child(self)
	self.set_owner(newparent)
	offset = get_parent().get_node("Protestor").offset
